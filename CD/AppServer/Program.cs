﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using CD;

namespace AppServer
{
    class Program
    {

        private static List<TcpClient> tcpClients;

        static IPEndPoint EndPointIP = new IPEndPoint(IPAddress.Any, 9000);
        static TcpListener tcpListener;
        static void Main(string[] args)
        {

            Thread newServiceThread = new Thread(Start);
            newServiceThread.Start();

        }

        public static void Start()
        {

            tcpClients = new List<TcpClient>();

            tcpListener = new TcpListener(IPAddress.Any, 9000);
            tcpListener.Start();

            while (true)
            {
                Console.WriteLine("Aguardando conexão...");

                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                tcpClients.Add(tcpClient);
                Console.WriteLine("Ligado!");

                var childSocketThread = new Thread(() =>
                {
                    while (true)
                    {
                        byte[] byteMensagem = new byte[1024];

                        try { tcpClient.Client.Receive(byteMensagem); }catch(Exception r) { }
                        string message = Encoding.ASCII.GetString(byteMensagem);

                        Console.WriteLine(message);

                        foreach(TcpClient teste in tcpClients)
                        {

                            if (teste != tcpClient) tcpClient.Client.Send(byteMensagem);

                        }

                    }
                });
                childSocketThread.Start();

            }
        }

    }
}
