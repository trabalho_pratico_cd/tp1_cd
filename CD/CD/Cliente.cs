﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CD.Forms;

namespace CD
{
    public class Cliente
    {
        private TcpClient _tcpClient;
        private IPEndPoint _ipEndPoint;
        public string IpAddress { get; set; }
        public int Port { get; set; }
        private Chatroom chatroomForm;

        public TcpClient tcpClient { get => _tcpClient; }

        public Cliente(Chatroom form)
        {
            chatroomForm = form;
            //Start();
        }

        public void Start()
        {
            _ipEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.67"), 9000);
            _tcpClient = new TcpClient();
            _tcpClient.Connect(_ipEndPoint);
            
            while (true)
            {
                if (_tcpClient.Available > 0)
                {

                    byte[] buffer = new byte[1024];

                    _tcpClient.Client.Receive(buffer);

                    //Receive(_tcpClient.Client, buffer, 0, _tcpClient.Available, 10000);
                    string mensagem = Encoding.ASCII.GetString(buffer, 0, buffer.Length);

                    //todo: processar mensagem num método separado
                    /*User user = JsonConvert.DeserializeObject<User>(jsonString);
                    */
                    chatroomForm.Mensagens = mensagem;

                }
            }
        }

        public static void Receive(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {
            int startTickCount = Environment.TickCount;
            int received = 0;  // how many bytes is already received
            do
            {
                if (Environment.TickCount > startTickCount + timeout)
                    throw new Exception("Timeout.");
                try
                {
                    received += socket.Receive(buffer, offset + received, size - received, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                        ex.SocketErrorCode == SocketError.IOPending ||
                        ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                        // socket buffer is probably empty, wait and try again
                        Thread.Sleep(30);
                    }
                    else
                        throw ex;  // any serious error occurr
                }
            } while (received < size);
        }
    }
}
