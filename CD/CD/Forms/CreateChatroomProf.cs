﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class CreateChatroomProf : Form
    {

        //public static Chatroom novaChatroom;

        public CreateChatroomProf()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Chatroom novaChatroom = new Chatroom();

            int resultado = Chatrooms.createChatroom(textBox1.Text);

            if (resultado == 1) MessageBox.Show("Já existe uma Chatroom com esse nome, por favor introduza um nome diferente!");

            else
            {

                MessageBox.Show("A Chatroom foi criada com sucesso!");

                try
                {

                    CD.Forms.Menu.novoCriarChatProf.Hide();
                    if (checkBox1.Checked) Chatrooms.currentChatroom.UC = true;
                    else
                    {

                        Chatrooms.currentChatroom.UC = false;

                    }


                    novaChatroom.ShowDialog();
                    this.Close();
                    this.Dispose();
                    CD.Forms.Menu.novoCriarChatProf.Close();


                }
                catch(Exception t)
                {

                    Console.WriteLine(t.Message);
                }

                

            }

            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void CreateChatroomProf_Load(object sender, EventArgs e)
        {

        }
    }
}
