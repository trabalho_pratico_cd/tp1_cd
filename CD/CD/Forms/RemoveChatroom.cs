﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class RemoveChatroom : Form
    {
        public RemoveChatroom()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int resultado = Chatrooms.removeChatroom(textBox1.Text);

            if (resultado == 1) MessageBox.Show("Ocorreu um erro! (Nenhum texto foi introduzido)");

            else if (resultado == 2) MessageBox.Show("Ocorreu um erro! (Nenhuma das suas Chatrooms possui o código introduzido)");

            else
            {

                MessageBox.Show("A ChatRoom foi removida com sucesso!");
                this.Hide();
                Menu newMenu = new Menu();
                newMenu.ShowDialog();
                this.Close();

            }
        }

        private void RemoveChatroom_FormClosed(object sender, FormClosedEventArgs e)
        {

            this.Hide();
            Menu newMenu = new Menu();
            newMenu.ShowDialog();
            this.Close();

        }
    }
}
