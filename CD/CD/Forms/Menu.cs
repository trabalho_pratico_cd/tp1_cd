﻿using System;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class Menu : Form
    {

        public static CreateChatroom novoForm;
        public static AddChatroom newForm;
        public static Chatroom novaChatroom;
        public static CreateChatroomProf novoCriarChatProf;

        public Menu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Botão para sair
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Deseja sair da aplicação? (Se sim, será redirecionado para a página de Login)", "Sair", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                Utilizadores.currentUser = new Utilizador();

                this.Hide();
                Form1 login = new Form1();
                login.ShowDialog();
                this.Close();

            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Botão para alterar conta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            ChangeAccount alterarConta = new ChangeAccount();
            alterarConta.ShowDialog();
            this.Close();
        }


        /// <summary>
        /// Botão para apagar conta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {

            this.Hide();
            RemoveAccount removerConta = new RemoveAccount();
            removerConta.ShowDialog();
            this.Close();

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(listBox2.SelectedItem != null)
            {

                Chatrooms.selectChatroom(listBox2.SelectedItem.ToString());
                novaChatroom = new Chatroom();
                novaChatroom.ShowDialog();

            }
            
        }

        private void listBox2_DoubleClick(object sender, EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(listBox2.SelectedItem.ToString()))
            {

                Chatrooms.selectChatroom(listBox2.SelectedItem.ToString());

                novaChatroom = new Chatroom();
                novaChatroom.ShowDialog();

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            if(Utilizadores.currentUser.Estudante == false)
            {

                novoCriarChatProf = new CreateChatroomProf();
                novoCriarChatProf.ShowDialog();

            }

            else
            {

                novoForm = new CreateChatroom();
                novoForm.ShowDialog();

            }

        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chatrooms.selectChatroom(listBox2.SelectedItem.ToString());

            novaChatroom = new Chatroom();
            novaChatroom.ShowDialog();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

            Utilizadores.loadMenu();
            foreach (ChatRoom teste in Utilizadores.currentUser.conjuntoSalas)
                listBox2.Items.Add(teste.Nome);

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            ChangeAccount newForm = new ChangeAccount();
            newForm.ShowDialog();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {

            newForm = new AddChatroom();
            newForm.ShowDialog();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            RemoveChatroom newRemove = new RemoveChatroom();
            newRemove.ShowDialog();
            this.Close();
        }
    }
}
