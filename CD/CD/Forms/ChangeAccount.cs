﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class ChangeAccount : Form
    {
        public ChangeAccount()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Botão pare aplicar as alterações feitas à conta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {

            int resultado = Utilizadores.changeAccount(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text);

            if (resultado == 2) MessageBox.Show("Ocorreu um erro a alterar a sua conta! (Já existe uma conta com o Username introduzido)");

            else if (resultado == 3) MessageBox.Show("Ocorreu um erro a alterar a sua conta! (Já existe uma conta com o E-Mail introduzido)");

            else if (resultado == 4) MessageBox.Show("Ocorreu um erro a alterar a sua conta! (Nenhum texto foi introduzido!)");

            else
            {

                MessageBox.Show("A sua conta foi alterada com sucesso!");

                this.Hide();
                Menu newMenu = new Menu();
                newMenu.ShowDialog();
                this.Close();

            }

        }

        /// <summary>
        /// username
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// primeiro nome
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// ultimo nome
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// curso
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Botão para sair e voltar ao menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {

            this.Hide();
            Menu newMenu = new Menu();
            newMenu.ShowDialog();
            this.Close();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool resultado = Utilizadores.checkEmail(textBox6.Text);

            if (resultado == false) MessageBox.Show("Já existe uma conta com o E-Mail introduzido!");

            else if (string.IsNullOrWhiteSpace(textBox6.Text)) MessageBox.Show("Não foi introduzido nenhum E-mail!");

            else { MessageBox.Show("O E-Mail introduzido está disponível!"); }
        }

    }
}
