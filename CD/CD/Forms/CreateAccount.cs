﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class CreateAccount : Form
    {

        int checkbox1, checkbox2;

        public CreateAccount()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TextBox que contém o Username do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(textBox1.Text)) Utilizadores.currentUser.Username = textBox1.Text;

        }

        /// <summary>
        /// TextBox que contém a Password do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';

            if (!string.IsNullOrWhiteSpace(textBox2.Text)) Utilizadores.currentUser.Password = textBox2.Text;
        }

        /// <summary>
        /// TextBox que contém o Curso do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox5.Text)) Utilizadores.currentUser.Curso = textBox5.Text;
        }

        /// <summary>
        /// TextBox que contém o Primeiro Nome do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox3.Text)) Utilizadores.currentUser.PrimeiroNome = textBox3.Text;
        }

        /// <summary>
        /// TextBox que contém o Último Nome do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox4.Text)) Utilizadores.currentUser.UltimoNome = textBox4.Text;
        }

        /// <summary>
        /// Botão que verifica a disponibilidade do E-Mail introduzido pelo Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            bool resultado = Utilizadores.checkEmail(Utilizadores.currentUser.Email);

            if (resultado == false) MessageBox.Show("Já existe uma conta com o E-Mail introduzido!");

            else { MessageBox.Show("O E-Mail introduzido está disponível!"); }
        }

        /// <summary>
        /// Botão que irá ou não criar uma conta nova com os dados introduzidos pelo Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            int resultado = Utilizadores.createAccount(Utilizadores.currentUser, checkbox1, checkbox2);

            if (resultado == 2) MessageBox.Show("Ocorreu um erro a criar a sua conta! (Já existe uma conta com o E-mail introduzido)");

            else if (resultado == 3) MessageBox.Show("Ocorreu um erro a criar a sua conta! (Já existe uma conta com o Username introduzido)");

            else if (resultado == 1)
            {

                MessageBox.Show("A sua conta foi criada com sucesso! Faça login com os seus dados para aceder à aplicação!");

                this.Hide();
                Form1 login = new Form1();
                login.ShowDialog();
                this.Close();

            }

            else { MessageBox.Show("Ocorreu um erro a criar a sua conta! (Um ou mais campos não foram preenchidos)"); }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// TextBox que contém o E-Mail do Utilizador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox6.Text)) Utilizadores.currentUser.Email = textBox6.Text;
        }

        /// <summary>
        /// Botão para sair e voltar ao menu de Login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Utilizadores.currentUser = new Utilizador();

            this.Hide();
            Form1 login = new Form1();
            login.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// CheckBox que indica que o Utilizador é um Estudante
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) { Utilizadores.currentUser.Estudante = true; checkbox1 = 1; }
            else { checkbox1 = 0; }

        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (textBox2.PasswordChar == '*') textBox2.PasswordChar = '\0';

            else { textBox2.PasswordChar = '*'; }

        }

        /// <summary>
        /// CheckBox que indica que o Utilizador é um Professor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked) { Utilizadores.currentUser.Estudante = false; checkbox2 = 1; }
            else { checkbox2 = 0; }
                
        }
    }
}
