﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using CD.Forms;

namespace CD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //this.StartPosition = FormStartPosition.Manual;
            //this.Location = new Point(0, 0);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        internal static void Form1_UIThreadException(object sender, ThreadExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateAccount aux = new CreateAccount();
            this.Hide();
            aux.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// Botão de login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;
            bool resultado = Utilizadores.loginAccount(Utilizadores.currentUser, username, password);

            if (resultado == false) MessageBox.Show("Ocorreu um erro ao executar o login. Verifique se tem a Password e o Username corretos!");

            else
            {

                Forms.Menu aux = new Forms.Menu();
                this.Hide();
                MessageBox.Show("Login efetuado com sucesso!");
                aux.ShowDialog();
                this.Close();

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
        }
    }
}
