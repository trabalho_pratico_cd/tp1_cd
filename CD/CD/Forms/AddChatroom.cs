﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class AddChatroom : Form
    {
        public static Chatroom newChatroom;

        public AddChatroom()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            bool resultado = Chatrooms.addChatroom(Int32.Parse(textBox1.Text));

            if (resultado == false) MessageBox.Show("Erro! (Não existe nenhuma Chatroom com o código introduzido)");

            else
            {

                MessageBox.Show("A Chatroom foi adicionada com sucesso!");

                newChatroom = new Chatroom();
                
                CD.Forms.Menu.newForm.Hide();
                newChatroom.ShowDialog();
                CD.Forms.Menu.newForm.Close();


            }

        }
    }
}
