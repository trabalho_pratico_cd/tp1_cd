﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class CreateChatroom : Form
    {

        //public static Chatroom novaChatroom;

        public CreateChatroom()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //try { novaChatroom = new CD.Forms.Chatroom(); }catch(Exception v) { Console.WriteLine(v.Message); }

            

            int resultado = Chatrooms.createChatroom(textBox1.Text);

            if (resultado == 1) MessageBox.Show("Já existe uma Chatroom com esse nome, por favor introduza um nome diferente!");

            else
            {

                MessageBox.Show("A Chatroom foi criada com sucesso!");

                try
                {

                    Chatroom novaChatroom = new Chatroom();
                    CD.Forms.Menu.novoForm.Hide();
                    Chatrooms.currentChatroom.UC = false;
                    novaChatroom.ShowDialog();
                    CD.Forms.Menu.novoForm.Close();

                }
                catch(Exception t)
                {

                    Console.WriteLine(t.Message);
                }

                

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
