﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CD.Forms
{
    public partial class Chatroom : Form
    {

        Cliente cliente;
        private delegate void SafeCallDelegate(string text);

        public string Mensagens
        {
            set
            {

                //try { listBox1.Items.Add(value); } catch (Exception e) { listBox1.Items.Add(value); }
                //listBox1.Items.Add(value);

                WriteTextSafe(value);
                
            }
        }

        public Chatroom()
        {
            InitializeComponent();

            cliente = new Cliente(this);
            Thread networkServiceThread = new Thread(cliente.Start);
            networkServiceThread.Start();

            //cliente.Start();
        }

        public void WriteTextSafe(string text)
        {

            if (listBox1.InvokeRequired)
            {
                var d = new SafeCallDelegate(WriteTextSafe);
         
                this.Invoke((MethodInvoker)(() => listBox1.Items.Add(text)));
            }
            else
            {
                textBox1.Text = text;
            }

        }
        
        private void button1_Click(object sender, EventArgs e)
        {

            if(!string.IsNullOrWhiteSpace(textBox1.Text))
            {

                string mensagem = Utilizadores.currentUser.PrimeiroNome + " " + Utilizadores.currentUser.UltimoNome + ":" + textBox1.Text;
                byte[] bytes = Encoding.ASCII.GetBytes(mensagem);
                /*Byte[] data = System.Text.Encoding.ASCII.GetBytes(mensagem);
                NetworkStream stream = cliente.tcpClient.GetStream();
                stream.Write(data, 0, data.Length);

                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                Console.WriteLine("Received: {0}", responseData);

                // Close everything.
                stream.Close();*/

                /*var d = new SafeCallDelegate(cliente.tcpClient.Client.Send(bytes));
                textBox1.Invoke(d, new object[] { text });*/

                try { cliente.tcpClient.Client.Send(Encoding.ASCII.GetBytes(mensagem)); }catch(NullReferenceException s) { }
                listBox1.Items.Add(mensagem);

            }

        }

        /// <summary>
        /// Void que irá executar instruções quando o form fechar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Chatroom_FormClosing(object sender, FormClosingEventArgs e)
        {

            Chatrooms.currentChatroom = new Chatroom();

            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == "novaChatroom" || frm.Name == "newChatroom")
                {
                    frm.Hide();
                    frm.Close();
                }
            }

            cliente.tcpClient.Close();

        }

        private void Chatroom_Load_1(object sender, EventArgs e)
        {

            Label customLabel = new Label();

            if (Chatrooms.currentChatroom.UC == true)
            {

                customLabel.Text = "Unidade Curricular:" + Chatrooms.currentChatroom.Nome + " || Docente:" + Chatrooms.currentChatroom.NomeAdmin+" || Codigo:"+Chatrooms.currentChatroom.Codigo.ToString();
                customLabel.Location = new Point(32, 18);
                customLabel.Font = new Font("Microsoft Sans Serif", 10);
                customLabel.AutoSize = true;

            }

            else
            {

                customLabel.Text = "Nome da Chatroom:" + Chatrooms.currentChatroom.Nome + " || Criador da Chatroom:" + Chatrooms.currentChatroom.NomeAdmin + " || Codigo:" + Chatrooms.currentChatroom.Codigo.ToString();
                customLabel.Location = new Point(32, 18);
                customLabel.Font = new Font("Microsoft Sans Serif", 10);
                customLabel.AutoSize = true;

            }

            this.Controls.Add(customLabel);

            //Cliente_Servidor.Server server = new Cliente_Servidor.Server();
            //server.Start();

        }

        private void Chatroom_FormClosed(object sender, FormClosedEventArgs e)
        {

            try
            {
                Chatrooms.currentChatroom = new Chatroom();

                Menu newMenu = new Menu();
                newMenu.ShowDialog();
            }catch(Exception t)
            {
                Console.WriteLine(t.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu newMenu = new Menu();
            newMenu.ShowDialog();
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void addItemListbox(string message)
        {

            listBox1.Items.Add(message);

        }


    }
}
