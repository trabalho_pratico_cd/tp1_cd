﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CD.Forms;

namespace CD
{
    [Serializable]
    public class ChatRoom
    {

        #region Variáveis

        string nomeAdmin, nome;
        int codigo;
        bool uc;

        #endregion

        #region Propriedades

        public string Nome { get => nome; set => nome = value; }

        public string NomeAdmin { get => nomeAdmin; set => nomeAdmin = value; }

        public int Codigo { get => codigo; set => codigo = value; }

        public bool UC { get => uc; set => uc = value; }

        #endregion

        #region Construtores

        /// <summary>
        /// Construtor default da classe
        /// </summary>
        public ChatRoom()
        {

            this.Nome = default(string);
            this.NomeAdmin = default(string);
            this.Codigo = default(int);
            this.UC = default(bool);

        }

        /// <summary>
        /// Construtor com instruções específicas para a criação de uma nova Chatroom
        /// </summary>
        /// <param name="nome">Nome da Chatroom</param>
        /// <param name="nomeAdmin">Nome do Criador da Chatroom</param>
        /// <param name="codigo">Codigo para aceder à Chatroom</param>
        /// <param name="uc">Booleano que indica se a sala de chat é de uma Unidade Curricular ou não</param>
        public ChatRoom(string nome, string nomeAdmin, int codigo, bool uc)
        {

            this.Nome = nome;
            this.NomeAdmin = nomeAdmin;
            this.Codigo = codigo;
            this.UC = uc;

        }

        public static implicit operator ChatRoom(Forms.Chatroom v)
        {
            //throw new NotImplementedException();
            return v;
        }

        #endregion
    }
}
