﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CD
{
    [Serializable]
    public class Chatrooms
    {

        static List<ChatRoom> roomsList;
        private const string DATAFILE = "Chatrooms.dat";
        public static ChatRoom currentChatroom = new ChatRoom();

        #region Construtores

        static Chatrooms()
        {

            roomsList = new List<ChatRoom>();
            CarregaFicheiro();

        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Recebe o nome de uma das chatrooms em que o utilizador se encontra e retorna os valores dessa mesma chatroom para a variavel "Chatrooms.currentChatroom"
        /// </summary>
        /// <param name="nomeChatroom"></param>
        public static void selectChatroom(string nomeChatroom)
        {

            if (!string.IsNullOrWhiteSpace(nomeChatroom))
            {

                foreach (ChatRoom teste in roomsList)
                {

                    if (teste.Nome == nomeChatroom)
                    {

                        Chatrooms.currentChatroom.Nome = teste.Nome;
                        Chatrooms.currentChatroom.Codigo = teste.Codigo;
                        Chatrooms.currentChatroom.NomeAdmin = teste.NomeAdmin;
                        Chatrooms.currentChatroom.UC = teste.UC;
                        break;
                    }

                }

            }

        }

        /// <summary>
        /// Método que adiciona uma chatroom já existente ao conjunto de salas do utilizador
        /// </summary>
        /// <param name="codigo">Código da sala que o Utilizador deseja adicionar</param>
        /// <returns>
        /// true-sucesso
        /// false-a chatroom não existe (erro)
        /// </returns>
        public static bool addChatroom(int codigo)
        {

            foreach (ChatRoom teste in roomsList)
                if (teste.Codigo == codigo)
                {

                    Utilizadores.currentUser.conjuntoSalas.Add(teste);

                    currentChatroom.Codigo = teste.Codigo;
                    currentChatroom.Nome = teste.Nome;
                    currentChatroom.NomeAdmin = teste.NomeAdmin;
                    currentChatroom.UC = teste.UC;

                    return true;
                }

            return false;

        }

        /// <summary>
        /// Método que verifica se já existe uma chatroom com o nome introduzido pelo utilizador e irá ou não criar a dita chatroom com um código aleatório de 4 dígitos
        /// </summary>
        /// <param name="nomeChatroom">Nome da Chatroom introduzido pelo utilizador</param>
        /// <returns>
        /// 1- erro
        /// 0- sucesso
        /// </returns>
        public static int createChatroom(string nomeChatroom)
        {

            ChatRoom novaChatroom = new ChatRoom();

            Random r = new Random();
            novaChatroom.Codigo = r.Next(1000,9999);

            try
            {
                foreach (ChatRoom teste in roomsList)
                    if (teste.Codigo == novaChatroom.Codigo) createChatroom(nomeChatroom);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                foreach (ChatRoom teste1 in roomsList)
                    if (teste1.Nome == nomeChatroom) return 1;
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            novaChatroom.NomeAdmin = Utilizadores.currentUser.PrimeiroNome + " " + Utilizadores.currentUser.UltimoNome;
            novaChatroom.Nome = nomeChatroom;
            Chatrooms.currentChatroom = novaChatroom;

            try
            {
                roomsList.Add(novaChatroom);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Utilizadores.currentUser.conjuntoSalas.Add(novaChatroom);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Utilizadores.loadMenu();
            
            EscreveFicheiro();
            return 0;

        }

        /// <summary>
        /// Método que irá remover uma ChatRoom da lista de Salas do current User
        /// </summary>
        /// <param name="codigoChatroom">Código introduzido pelo Utilizador, remetenta à sala que deseja remover</param>
        /// <returns>
        /// 1-Nenhum texto foi introduzido
        /// 2-Não existe nenhuma ChatRoom com o código introduzido
        /// 0-Sucesso
        /// </returns>
        public static int removeChatroom(string codigoChatroom)
        {

            if (string.IsNullOrWhiteSpace(codigoChatroom)) return 1;

            int codigo = Int32.Parse(codigoChatroom);

            foreach(ChatRoom teste in Utilizadores.currentUser.conjuntoSalas)
                if(teste.Codigo == codigo)
                {

                    Utilizadores.currentUser.conjuntoSalas.Remove(teste);
                    return 0;

                }

            return 2;

        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Carrega ficheiro que contém objecto (que por sua vez contém os dados da lista) guardado de forma persistente para a aplicação
        /// </summary>
        private static void CarregaFicheiro()
        {

            // Declaração de variáveis locais
            string caminhoFicheiro;
            Stream stream;

            // Definir caminho absoluto de onde o ficheiro de texto será carregado
            caminhoFicheiro = Environment.CurrentDirectory + "\\" + DATAFILE;

            // Se o ficheiro alvo não existir, ignorar o resto das iterações do presente método
            if (File.Exists(caminhoFicheiro) == false) return;

            // Inicializar stream de leitura através da abertura do ficheiro onde os dados estão guardados  
            stream = File.Open(caminhoFicheiro, FileMode.Open);

            // Inicializar classe responsável por serializar dados em binário
            var serializador = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            try
            {

                // Carregar binário "desserializado" para o objecto que contém os dados
                roomsList = (List<ChatRoom>)serializador.Deserialize(stream);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

            }

            // Fechar stream de leitura de modo a libertar os recursos do sistema
            stream.Close();

        }

        /// <summary>
        /// Guarda objecto que contém a lista em binário
        /// </summary>
        private static void EscreveFicheiro()
        {

            // Declaração de variáveis locais
            string caminhoFicheiro;
            Stream stream;

            // Definir caminho absoluto onde o ficheiro de texto será criado e escrito
            caminhoFicheiro = Environment.CurrentDirectory + "//" + DATAFILE;

            // Inicializar stream de escrita através da criação do ficheiro onde serão guardados
            // Caso o ficheiro já exista, será reescrito 
            stream = File.Create(caminhoFicheiro);

            // Inicializar classe responsável por serializar os dados em binário
            var serializador = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            try
            {

                // Serializar objecto que contém os dados em binário
                serializador.Serialize(stream, roomsList);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

            }

            // Fechar stream de escrita de modo a libertar os recursos do sistema
            stream.Close();

        }

        #endregion

    }
}
