﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace CD
{
    /// <summary>
    /// Classe que define o Utilizador
    /// </summary>
    [Serializable]
    public class Utilizador
    {

        #region Variáveis

        string primeiroNome, ultimoNome, curso, userName, password, email;
        bool estudante;
        public List<ChatRoom> conjuntoSalas = new List<ChatRoom>();

        #endregion

        #region Propriedades

        public string PrimeiroNome { get => primeiroNome; set => primeiroNome = value; }

        public string UltimoNome { get => ultimoNome; set => ultimoNome = value; }

        public string Curso { get => curso; set => curso = value; }

        public string Username { get => userName; set => userName = value; }

        public string Password { get => password; set => password = value; }

        public string Email { get => email; set => email = value; }

        public bool Estudante { get => estudante; set => estudante = value; }

        #endregion

        #region Construtores

        /// <summary>
        /// Construtor default da classe
        /// </summary>
        public Utilizador()
        {

            this.PrimeiroNome = default(string);
            this.UltimoNome = default(string);
            this.Curso = default(string);
            this.Username = default(string);
            this.Password = default(string);

        }

        /// <summary>
        /// Construtor que irá conter a informação do utilizador
        /// </summary>
        /// <param name="firstName">Primeiro Nome do Utilizador</param>
        /// <param name="lastName">Último Nome do Utilizador</param>
        /// <param name="curso">Curso do Utilizador</param>
        /// <param name="userName">Username do Utilizador</param>
        /// <param name="password">Password do Utilizador</param>
        public Utilizador(string firstName, string lastName, string curso, string userName, string password)
        {

            this.PrimeiroNome = firstName;
            this.UltimoNome = lastName;
            this.Curso = curso;
            this.Username = userName;
            this.Password = password;

        }

        #endregion

    }
}
