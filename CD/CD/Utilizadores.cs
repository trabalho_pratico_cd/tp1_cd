﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CD
{
    [Serializable]
    public class Utilizadores
    {
        static List<Utilizador> userList;
        private const string DATAFILE = "Utilizadores.dat";
        public static Utilizador currentUser = new Utilizador();

        #region Construtores

        static Utilizadores()
        {
            userList = new List<Utilizador>();
            CarregaFicheiro();
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Função que faz verificações e adiciona ou não o novo Utilizador à lista de Utilizadores
        /// </summary>
        /// <param name="currentUser">Utilizador usado para o login</param>
        /// <returns></returns>
        public static int createAccount(Utilizador currentUser, int checkbox1, int checkbox2)
        {

            if (!checkEmail(currentUser.Email)) return 2;

            if (userList.Count > 0)
            {

                foreach (Utilizador teste in userList)
                    if (currentUser.Username == teste.Username) return 3;             

            }

            if (string.IsNullOrEmpty(currentUser.Curso) == true || string.IsNullOrEmpty(currentUser.Email) == true || string.IsNullOrEmpty(currentUser.Password) == true || string.IsNullOrEmpty(currentUser.PrimeiroNome) == true || string.IsNullOrEmpty(currentUser.UltimoNome) == true || string.IsNullOrEmpty(currentUser.Username) == true || (checkbox1 != 1 && checkbox2 != 1)) return 4;

            userList.Add(currentUser);
            EscreveFicheiro();

            return 1;
        }

        /// <summary>
        /// Função que permitirá saber se já existe alguém com um e-mail igual ao introduzido pelo Utilizador
        /// </summary>
        /// <param name="email">E-Mail introduzido pelo Utilizador</param>
        /// <returns></returns>
        public static bool checkEmail(string email)
        {

            foreach (Utilizador teste in userList)
                if (teste.Email == email) return false;

            return true;
        }

        /// <summary>
        /// Função que executa o login
        /// </summary>
        /// <param name="username">Username introzudido pelo Utilizador</param>
        /// <param name="password">Password introduzida pelo Utilizador</param>
        /// <returns></returns>
        public static bool loginAccount(Utilizador currentUser, string username, string password)
        {

            foreach (Utilizador teste in userList)
                if (teste.Username == username && teste.Password == password)
                {
                    currentUser.Username = teste.Username;
                    currentUser.UltimoNome = teste.UltimoNome;
                    currentUser.Estudante = teste.Estudante;
                    currentUser.Curso = teste.Curso;
                    currentUser.Email = teste.Email;
                    currentUser.PrimeiroNome = teste.PrimeiroNome;
                    currentUser.Password = teste.Password;

                    if (teste.conjuntoSalas != null) currentUser.conjuntoSalas = teste.conjuntoSalas.ToList();

                    return true;
                }

            return false;

        }

        /// <summary>
        /// Função que remove a conta do Utilizador "currentUser" caso a password introduzida seja a mesma que a do Utilizador
        /// </summary>
        /// <param name="currentUser">Utilizador que está a usar a app</param>
        /// <param name="password">Password introduzida pelo Utilizador</param>
        /// <returns>
        /// true- sucesso
        /// false- erro
        /// </returns>
        public static bool deleteAccount(Utilizador currentUser, string password)
        {

            if (currentUser.Password != password) return false;

            try
            {

                foreach (Utilizador teste in userList)
                    if (currentUser.Username == teste.Username && currentUser.Password == teste.Password) userList.Remove(teste);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

            }

            EscreveFicheiro();
                
            return true;

        }

        /// <summary>
        /// Método que altera as Informações do Utilizador
        /// </summary>
        /// <param name="username">Novo username introduzido</param>
        /// <param name="password">Nova password introduzida</param>
        /// <param name="primeiroNome">Novo primeiro nome introduzido</param>
        /// <param name="ultimoNome">Novo último nome introduzido</param>
        /// <param name="curso">Novo nome de curso introduzido</param>
        /// <param name="email">Novo email introduzido</param>
        /// <returns>
        /// 1- sucesso
        /// 2- erro, o novo username introduzido já existe
        /// 3- erro, o novo email introduzido já existe
        /// 4- nenhum texto foi introduzido
        /// </returns>
        public static int changeAccount(string username, string password, string primeiroNome, string ultimoNome, string curso, string email)
        {

            bool possivel = true;

            string emailAntigo = Utilizadores.currentUser.Email;

            foreach (Utilizador test in userList)
                if (test.Email == emailAntigo)
                {

                    if (!string.IsNullOrWhiteSpace(username))
                    {

                        foreach (Utilizador teste in userList)
                            if (teste.Username == username) possivel = false;

                        if (possivel == true) { currentUser.Username = username; test.Username = username; }

                        else { return 2; }

                    }

                    if (!string.IsNullOrWhiteSpace(password)) { currentUser.Password = password; test.Password = password; }

                    if (!string.IsNullOrWhiteSpace(primeiroNome)) { currentUser.PrimeiroNome = primeiroNome; test.PrimeiroNome = primeiroNome; }

                    if (!string.IsNullOrWhiteSpace(ultimoNome)) { currentUser.UltimoNome = ultimoNome; test.UltimoNome = ultimoNome; }

                    if (!string.IsNullOrWhiteSpace(curso)) { currentUser.Curso = curso; test.Curso = curso; }

                    if (!string.IsNullOrWhiteSpace(email))
                    {

                        possivel = Utilizadores.checkEmail(email);

                        if (possivel == true) { currentUser.Email = email; test.Email = email; }

                        else { return 3; }

                    }

                }

            if (string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(password) && string.IsNullOrWhiteSpace(primeiroNome) && string.IsNullOrWhiteSpace(ultimoNome) && string.IsNullOrWhiteSpace(curso) && string.IsNullOrWhiteSpace(email)) return 4;

            EscreveFicheiro();

            return 1;
        }

        /// <summary>
        /// Void que irá manter a permanência do conjunto de Salas dos utilizadores, irá ser executado quando o form do menu carregar
        /// </summary>
        public static void loadMenu()
        {

            foreach(Utilizador teste in userList)
            {

                if (teste.Password == currentUser.Password && teste.Username == currentUser.Username && currentUser.conjuntoSalas != null) teste.conjuntoSalas = currentUser.conjuntoSalas.ToList();

            }

            EscreveFicheiro();

        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Carrega ficheiro que contém objecto (que por sua vez contém os dados da lista) guardado de forma persistente para a aplicação
        /// </summary>
        private static void CarregaFicheiro()
        {

            // Declaração de variáveis locais
            string caminhoFicheiro;
            Stream stream;

            // Definir caminho absoluto de onde o ficheiro de texto será carregado
            caminhoFicheiro = Environment.CurrentDirectory + "\\" + DATAFILE;

            // Se o ficheiro alvo não existir, ignorar o resto das iterações do presente método
            if (File.Exists(caminhoFicheiro) == false) return;

            // Inicializar stream de leitura através da abertura do ficheiro onde os dados estão guardados  
            stream = File.Open(caminhoFicheiro, FileMode.Open);

            // Inicializar classe responsável por serializar dados em binário
            var serializador = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            try
            {

                // Carregar binário "desserializado" para o objecto que contém os dados
                userList = (List<Utilizador>)serializador.Deserialize(stream);

            }
            catch(Exception e)
            {

                Console.WriteLine(e.Message);

            }

            // Fechar stream de leitura de modo a libertar os recursos do sistema
            stream.Close();

        }

        /// <summary>
        /// Guarda objecto que contém a lista em binário
        /// </summary>
        private static void EscreveFicheiro()
        {

            // Declaração de variáveis locais
            string caminhoFicheiro;
            Stream stream;

            // Definir caminho absoluto onde o ficheiro de texto será criado e escrito
            caminhoFicheiro = Environment.CurrentDirectory + "\\" + DATAFILE;

            // Inicializar stream de escrita através da criação do ficheiro onde serão guardados
            // Caso o ficheiro já exista, será reescrito 
            stream = File.Create(caminhoFicheiro);

            // Inicializar classe responsável por serializar os dados em binário
            var serializador = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            try
            {

                // Serializar objecto que contém os dados em binário
                serializador.Serialize(stream, userList);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);

            }

            // Fechar stream de escrita de modo a libertar os recursos do sistema
            stream.Close();

        }

        #endregion
    }
}
